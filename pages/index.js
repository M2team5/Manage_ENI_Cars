import Navbar from "../components/homepage/Navbar"
import Header from "../components/homepage/Header"
import Features from "../components/homepage/Features"

export default function Home() {

  return (

    <>
      <Navbar header />
      <Features />
    </>

  )
}
